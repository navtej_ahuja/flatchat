from django.contrib import admin
from secondapp.models import student,contact_us,category,register_model,add_product,cart,order

admin.site.site_header="My Website Second Project"

class studentAdmin(admin.ModelAdmin):
    # fields = ["name", "email", "roll_no"]
    list_display = ["name","email","roll_no","fee","gender","address","is_registered"]
    search_fields = ["roll_no","name"]
    list_filter = ["name","gender"]
    list_editable = ["email"]

class contact_usAdmin(admin.ModelAdmin):
    # fields = ["contact_number","name"]
    list_display = ["id","name","contact_number","subject","message","added_on"]
    search_fields = ["name"]
    list_filter = ["added_on","name"]
    list_editable = ["contact_number"]

class categoryAdmin(admin.ModelAdmin):
    list_display = ["id","cat_name","description","added_on"]


admin.site.register(student,studentAdmin)
admin.site.register(contact_us,contact_usAdmin)
admin.site.register(category,categoryAdmin)
admin.site.register(register_model)
admin.site.register(add_product)
admin.site.register(cart)
admin.site.register(order)
