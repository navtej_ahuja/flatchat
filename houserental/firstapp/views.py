from django.shortcuts import render
from django.http import HttpResponse,HttpResponseRedirect
# from firstapp.models import contact_us,signup_model,category
from django.contrib.auth.models import User
# from django.contrib.auth import login,authenticate,logout
from django.contrib.auth.decorators import login_required
# import requests
# from firstapp.forms import add_property_form

def home(request):
    # cats = category.objects.all()
    return render(request,"home.html",{"category":cats})

def signin(request):
    return render(request,"signin.html")

def aboutus(request):
    return render(request,"aboutus.html")

def category_type(request):
    # cats = category.objects.all()
    return render(request,"category.html",{"category":cats})

def contact(request):
    # if request.method=="POST":
    #     msg = request.POST["message"]
    #     nm = request.POST["name"]
    #     em = request.POST["email"]
    #     sub = request.POST["subject"]

    #     data = contact_us(message=msg,name=nm,email=em,subject=sub)
    #     data.save()
    #     res = "Dear {} thanks for your feedback".format(nm)
    #     return render(request,"contact.html",{"status": res})

    return render(request,"contact.html")

def signup(request):
    if request.method=="POST":
        fname = request.POST["first"]
        last = request.POST["last"]
        un = request.POST["uname"]
        pwd = request.POST["password"]
        em = request.POST["email"]
        con = request.POST["contact"]
        tp = request.POST["utype"]
        # print(request.POST)

        usr = User.objects.create_user(un,em,pwd)
        usr.first_name = fname
        usr.last_name = last

        if tp == "sell":
            usr.is_staff = True
        usr.save()

        sig = signup_model(user=usr,contact_number=con)
        sig.save()
        return render(request,"signup.html",{"status": "Mr./Mrs {} your account created successfully".format(fname)})
    return render(request,"signup.html")

def check_user(request):
    if request.method=="GET":
        un = request.GET["usern"]
        check = User.objects.filter(username=un)
        if len(check) == 1:
            return HttpResponse("Exists")
        else:
            return HttpResponse("Not Exists")

def user_login(request):
    if request.method=="POST":
        un = request.POST["username"]
        pwd = request.POST["pwd"]

        user = authenticate(username=un,password=pwd)
        if user:
            login(request,user)
            if user.is_superuser:
                return HttpResponseRedirect("/admin")
            else:
                return HttpResponseRedirect("/customer_dashboard")
            # if user.is_active:
            #     return HttpResponseRedirect("/customer_dashboard")
        else:
            return render(request,"home.html",{"status":"Invaid Username"})
        
    return HttpResponse("Called")

def edit_profile(request):
    context = {}
    check = signup_model.objects.filter(user__id=request.user.id)
    if len(check)>0:
      
        data = signup_model.objects.get(user__id=request.user.id)
        context["data"] = data
    if request.method=="POST":
        # print(request.FILES)
        fn = request.POST["fname"]
        ln = request.POST["lname"]
        em = request.POST["email"]
        con = request.POST["contact"]
        age = request.POST["age"]
        gen = request.POST["gender"]
        ct = request.POST["city"]
        occ = request.POST["occ"]
        abt = request.POST["about"]

        usr = User.objects.get(id=request.user.id)
        usr.first_name = fn
        usr.last_name = ln
        usr.email = em
        usr.save()

        data.contact_number = con
        data.age = age
        data.city = ct
        data.occupation = occ
        data.gender = gen
        data.about = abt
        data.save()

        if "image" in request.FILES:
            img = request.FILES["image"]
            data.profile_picture = img
            data.save()

        context["status"] = "Changes Saved Successfully"
    return render(request,"edit_profile.html",context)

def change_password(request):
    context = {}
    ch = signup_model.objects.filter(user__id=request.user.id)
    if len(ch)>0:
        data = signup_model.objects.get(user__id=request.user.id)
        context["data"] = data
    if request.method=="POST":
        current = request.POST["cpwd"]
        new_pas = request.POST["npwd"]
        
        user = User.objects.get(id=request.user.id)
        un = user.username
        check = user.check_password(current)
        
        if check==True:
            user.set_password(new_pas)
            user.save()
            user = User.objects.get(username=un)
            login(request,user)
            context["msz"] = "Password Change successfully"
            context["col"] = "alert-success"
        else:
            context["msz"] = "Incorrect Current Password"
            context["col"] = "alert-danger"

    return render(request,"change_password.html",context)

@login_required
def customer_dashboard(request):
    context = {}
    check = signup_model.objects.filter(user__id=request.user.id)
    if len(check)>0:
        data = signup_model.objects.get(user__id=request.user.id)
        context["data"] = data
    return render(request,"customer_dashboard.html",context)

@login_required
def seller_dashboard(request):
    return render(request,"seller_dashboard.html")

@login_required
def user_logout(request):
    logout(request)
    return HttpResponseRedirect("/")


def property_detail(request):
    return render(request,"property_detail.html")

def properties(request):
    return render(request,"property.html")

def add_property_view(request):
    # context = {}
    # check = signup_model.objects.filter(user__id=request.user.id)
    # if len(check)>0:
      
    #     data = signup_model.objects.get(user__id=request.user.id)
    #     context["data"] = data
    #     form = add_property_form()
    #     if request.method=="POST":
    #         form = add_property_form(request.POST,request.FILES)
    #         if form.is_valid():
    #             data = form.save(commit=False)
    #             login_user = User.objects.get(username=request.user.username)
    #             data.seller = login_user
    #             data.save()
    #             context["status"] = "{} Added successfully".format(data.property_name)
    #     context["form"] = form
        
    return render(request,"add_property.html")


