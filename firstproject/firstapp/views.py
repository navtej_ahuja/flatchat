from django.shortcuts import render
from django.http import HttpResponse
import requests

def first(request):
    return render(request,"first.html")

def about(r):
    return HttpResponse("<h1 style='color:red;'>This Is Anathor Django View </h1>")

def index(request):
    str = "I am from views file"
    data = ["red","green","blue","Magenta"]
    context = {"colors": data,"string":str}
    return render(request,"index.html",context)

def countries(request):
    URL = 'https://restcountries.eu/rest/v2/all'
    data = requests.get(URL).json()
    context = {"country": data}
    return render(request,"flag.html",context)